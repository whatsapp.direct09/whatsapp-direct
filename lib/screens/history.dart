import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:whatsapp_direct/utils/preferences.dart';

import '../constants/AppConstants.dart';

class MessageHistoryScreen extends StatefulWidget {
  const MessageHistoryScreen({Key? key, required this.tabController})
      : super(key: key);
  final TabController tabController;

  @override
  State<MessageHistoryScreen> createState() => _MessageHistoryScreenState();
}

class _MessageHistoryScreenState extends State<MessageHistoryScreen> {
  var messageHistory = [];

  @override
  initState() {
    super.initState();
    getHistoryMessage();
  }

  void getHistoryMessage() async {
    var messages = await getListPreferences(messageListPref);
    setState(() {
      messageHistory = messages;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
              shrinkWrap: true,
              itemCount: messageHistory.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    captureSelectMessage(currentMessagePref, {
                      'phoneNumber': messageHistory[index]['phoneNumber'],
                      'message': messageHistory[index]['message']
                    });
                    widget.tabController.animateTo(0);
                  },
                  child: ListTile(
                      leading: const Icon(Icons.list),
                      trailing: Text(
                        messageHistory[index]['message'],
                        style:
                            const TextStyle(color: Colors.green, fontSize: 15),
                      ),
                      title: Text(messageHistory[index]['phoneNumber'])),
                );
              })
        ],
      ),
    );
  }
}
