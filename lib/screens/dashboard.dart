import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:whatsapp_direct/constants/AppConstants.dart';
import 'package:whatsapp_direct/utils/constructor.dart';
import 'package:whatsapp_direct/utils/location.dart' as location;
import 'package:whatsapp_direct/utils/preferences.dart';

import '../ad_helper.dart';
import '../utils/api.dart';
import '../utils/validator.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({Key? key, required this.tabController})
      : super(key: key);
  final TabController tabController;

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  String finalMessage = '';
  int finalPhoneNumber = 0;
  late Position currentLocation;
  Uri uri = Uri();
  final TextEditingController phoneNumber = TextEditingController();
  final TextEditingController message = TextEditingController();

  late BannerAd _bannerAd;
  bool _isBannerAdReady = false;

  @override
  void initState() {
    super.initState();
    _bannerAd = BannerAd(
      adUnitId: AdHelper.bannerAdUnitId,
      request: const AdRequest(),
      size: AdSize.fullBanner,
      listener: BannerAdListener(
        onAdLoaded: (_) {
          setState(() {
            _isBannerAdReady = true;
          });
        },
        onAdFailedToLoad: (ad, err) {
          if (kDebugMode) {
            print('Failed to load a banner ad: ${err.message}');
          }
          _isBannerAdReady = false;
          ad.dispose();
        },
      ),
    );
    _bannerAd.load();

    widget.tabController.addListener(() {
      if (widget.tabController.index == 0) {
        updateSelectedMessage();
      }
    });
  }

  Future<void> updateSelectedMessage() async {
    var response = await getSelectedMessage(currentMessagePref);
    phoneNumber.value = phoneNumber.value.copyWith(
      text: response['phoneNumber'],
      // selection: TextSelection.collapsed(offset: response['phoneNumber'].length),
    );
    message.value = message.value.copyWith(
      text: response['message'],
      // selection: TextSelection.collapsed(offset: response['phoneNumber'].length),
    );
    setState(() {
      finalPhoneNumber = int.parse(response['phoneNumber']);
      finalMessage = response['message'];
    });
  }

  void sendWhatsappMessage() async {
    var whatsappMessage = constructWhatsappURL(finalPhoneNumber, finalMessage,
        currentLocation.latitude, currentLocation.longitude);
    redirectWhatsapp(whatsappMessage);
    saveMessage(messageListPref,
        {'phoneNumber': phoneNumber.text, 'message': finalMessage});
  }

  Future<void> getCurrentLocation() async {
    var current = await location.getCurrentLocation();
    setState(() {
      currentLocation = current;
    });
  }

  @override
  void dispose() {
    _bannerAd.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return SizedBox(
      height: height,
      width: width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                    margin: const EdgeInsets.all(8),
                    padding:
                        const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                    color: Colors.grey[300],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text('Enter phone number'),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          controller: phoneNumber,
                          onChanged: (number) {
                            var response = checkValidPhoneNumber(number);
                            if (kDebugMode) {
                              print(response);
                            }
                            setState(() {
                              if (response['isValid']) {
                                finalPhoneNumber =
                                    int.parse(response['phoneNumber']);
                              } else {
                                finalPhoneNumber = 0;
                              }
                            });
                          },
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Enter phone number',
                          ),
                        ),
                      ],
                    )),
                Container(
                    margin: const EdgeInsets.all(8),
                    padding:
                        const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                    color: Colors.grey[300],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text('Enter message'),
                        TextFormField(
                          controller: message,
                          onChanged: (message) {
                            setState(() {
                              finalMessage = message;
                            });
                          },
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Enter message',
                          ),
                        ),
                      ],
                    )),
                GestureDetector(
                  onTap: () {
                    getCurrentLocation();
                  },
                  child: Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(8),
                    color: Colors.blueAccent,
                    child: const Text('Attach current location'),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    sendWhatsappMessage();
                  },
                  child: Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(8),
                    color: Colors.blueAccent,
                    child: Text('Send to $finalPhoneNumber'),
                  ),
                ),
                // GestureDetector(
                //   onTap: () {
                //     print(Uri.decodeFull('https://www.google.com/maps/search/?api=1&query=47.5951518%2C-122.3316393'));
                //   },
                //   child: const Text(
                //     'tester',
                //     style: TextStyle(fontSize: 24),
                //   ),
                // )
              ],
            ),
          ),
          if (_isBannerAdReady)
            Align(
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                width: width,
                height: 40,
                child: AdWidget(ad: _bannerAd),
              ),
            ),
        ],
      ),
    );
  }
}
