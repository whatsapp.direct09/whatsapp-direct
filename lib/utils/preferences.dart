import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:whatsapp_direct/utils/convertor.dart';

Future<void> setPreferences(String key, var requestData,
    {String dataType = 'stringList'}) async {
  final prefs = await SharedPreferences.getInstance();
  if (dataType == 'stringList') {
    await prefs.setStringList(key, requestData);
  } else if (dataType == 'int') {
    await prefs.setInt(key, requestData);
  } else if (dataType == 'bool') {
    await prefs.setBool(key, requestData);
  } else if (dataType == 'double') {
    await prefs.setDouble(key, requestData);
  } else if (dataType == 'string') {
    await prefs.setString(key, requestData);
  }
}

Future getPreferences(String key,
    {String dataType = 'stringList'}) async {
  final prefs = await SharedPreferences.getInstance();
  Object? responseData;
  if (dataType == 'stringList') {
    responseData = prefs.getStringList(key);
  } else if (dataType == 'int') {
    responseData = prefs.getInt(key);
  } else if (dataType == 'bool') {
    responseData = prefs.getBool(key);
  } else if (dataType == 'double') {
    responseData = prefs.getDouble(key);
  } else if (dataType == 'string') {
    responseData = prefs.getString(key);
  }
  return responseData;
}

Future<void> removePreferences(key) async {
  final prefs = await SharedPreferences.getInstance();
  await prefs.remove(key);
}

Future<void> captureSelectMessage(key, requestData) async {
  setPreferences(key, jsonEncode(requestData), dataType: 'string');
}

Future<Map> getSelectedMessage(key) async {
  var selectedMessage = await getPreferences(key, dataType: 'string') ?? {};
  return jsonDecode(selectedMessage);
}

Future<void> saveMessage(key, requestData,
    {bool iterableData = false}) async {
  var previousData = await getListPreferences(key);
  if (previousData.isEmpty) {
    if (iterableData) {
      previousData.addAll(requestData);
    } else {
      previousData.add(requestData);
    }
  } else {
    if (iterableData) {
      // check validation, if required.
      previousData.addAll(requestData);
    } else {
      for (int i = 0; i < previousData.length; i++) {
        if (previousData[i]['phoneNumber'] == requestData['phoneNumber']) {
          previousData[i] = requestData;
          break;
        } else {
          previousData.add(requestData);
        }
      }
    }
  }
  // Encode List to List<String>
  setPreferences(key, encodeList(previousData));
}

Future<List> getListPreferences(key) async {
  var previousData = await getPreferences(key) ?? [];
  // Decode List<String> to List
  return decodeList(previousData);
}