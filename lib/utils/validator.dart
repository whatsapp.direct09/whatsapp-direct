Map checkValidPhoneNumber(String phoneNumber) {
  if (phoneNumber.isEmpty) {
    return {
      'isValid': false,
      'phoneNumber': phoneNumber
    };
  }

  phoneNumber = phoneNumber.trim();
  phoneNumber = phoneNumber.replaceAll(RegExp(r'^0+(?=.)'), '');
  if (phoneNumber.startsWith('+')) {
    phoneNumber = phoneNumber.substring(3);
  }
  RegExp regEx = RegExp(r'^((\+?[9]?[1]?[\s]?)|0)([6-9](\d{9}))$');
  if (regEx.hasMatch(phoneNumber)) {
    phoneNumber = phoneNumber.substring(phoneNumber.length-10);
    return {
      'isValid': true,
      'phoneNumber': phoneNumber
    };
  } else {
    return {
      'isValid': false,
      'phoneNumber': phoneNumber
    };
  }
}