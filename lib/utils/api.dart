import 'package:flutter/foundation.dart';
import 'package:url_launcher/url_launcher.dart';

void redirectWhatsapp(whatsappMessage) {
  var uri = Uri.parse(whatsappMessage);
  if (kDebugMode) {
    print(whatsappMessage);
    print(uri);
  }
  launchUrl(uri);
}
