import 'dart:io';

String constructWhatsappURL(phoneNumber, finalMessage, double latitude, double longitude) {
  var message = '';
  var encodedMapsURL = '';
  if (latitude.isFinite && longitude.isFinite) {
    var gMapURL = constructGMapURL(latitude.toString(), longitude.toString());
    encodedMapsURL = Uri.encodeComponent(gMapURL);
  }
  if (Platform.isAndroid) {
    message = "whatsapp://send?phone=91$phoneNumber&text=$finalMessage\n\n$encodedMapsURL";
  } else {
    message = "whatsapp://wa.me/91$phoneNumber/?text=$finalMessage\n\n$encodedMapsURL";
  }
  return message;
}

String constructGMapURL(var latitude, var longitude,
    {String urlType = 'direction'}) {
  if (urlType == 'direction') {
    return 'https://www.google.com/maps/dir/?api=1&destination=$latitude%2C$longitude';
  } else {
    return 'htt ps://www.google.com/maps/search/?api=1&query=$latitude%2C$longitude';
  }
}
