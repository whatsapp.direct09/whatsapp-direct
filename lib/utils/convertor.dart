import 'dart:convert';

List<String> encodeList(List data) {
  List<String> encodedList = [];
  for (int i = 0; i < data.length; i++) {
    encodedList.add(jsonEncode(data[i]));
  }
  return encodedList;
}

List decodeList(List data) {
  List decodeList = [];
  for (int i = 0; i < data.length; i++) {
    decodeList.add(jsonDecode(data[i]));
  }
  return decodeList;
}
